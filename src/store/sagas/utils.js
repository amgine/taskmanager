import { FIND_BY_ELEMENTS } from 'constants/index'

export function getDataByCriteria(tasks, subTasks, filter = {}) {
  if (filter[FIND_BY_ELEMENTS.TITLE]) {
    return [...tasks, ...subTasks].filter(
      ({ title }) => title.toLowerCase().indexOf(filter[FIND_BY_ELEMENTS.TITLE].toLowerCase()) > -1,
    )
  }
  // flow when filter with label
  const label = filter[FIND_BY_ELEMENTS.LABEL]

  const tasksIds = subTasks.reduce((acc, subTask) => {
    if (subTask.labels.includes(label)) {
      acc.push(subTask.taskId)
    }

    return acc
  }, [])

  return tasks.filter(task => tasksIds.includes(task.id))
}

export const filterSubTasksForRemove = (subtasksInStore, subTaskId) =>
  subtasksInStore.filter(subTask => subTask.id !== subTaskId)
