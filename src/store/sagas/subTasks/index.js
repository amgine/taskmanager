import { takeLatest, call, put, select } from 'redux-saga/effects'
import { fetchSubTasks, deleteSubtask } from 'api/subTasks'
import { actions as subTasks, getActiveTaskId, getSubTasksData } from 'store/modules/subtasks'
import { removeTask } from '../tasks'
import { filterSubTasksForRemove } from '../utils'

function* fetchSubTasksSaga({ payload: id }) {
  try {
    yield put(subTasks.loadingStart())

    const currentActiveId = yield select(getActiveTaskId)

    if (id === currentActiveId) {
      yield put(subTasks.setActiveTask(null))
      yield put(subTasks.setSubtasks([]))

      return
    }

    yield put(subTasks.setActiveTask(id))
    const data = yield call(fetchSubTasks, id)

    // console.log(data)
    yield put(subTasks.setSubtasks(data))
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  } finally {
    yield put(subTasks.loadingDone())
  }
}

export function* deleteSubTaskSaga({ payload: subTaskId }) {
  try {
    yield put(subTasks.deletingStart())

    yield call(deleteSubtask, subTaskId)

    const subtasksInStore = yield select(getSubTasksData)

    const changedSubTasks = yield call(filterSubTasksForRemove, subtasksInStore, subTaskId)

    yield put(subTasks.setSubtasks(changedSubTasks))

    const subTs = yield select(getSubTasksData)

    if (subTs.length) {
      return
    }

    yield call(removeTask)
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  } finally {
    yield put(subTasks.deletingDone())
  }
}

export default function* root() {
  yield takeLatest(subTasks.fetchSubTasks, fetchSubTasksSaga)
  yield takeLatest(subTasks.deleteSubTask, deleteSubTaskSaga)
}
