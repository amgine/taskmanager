import { testSaga } from 'redux-saga-test-plan'
import { deleteSubtask } from 'api/subTasks'
import { actions as subTasksActions, getSubTasksData } from 'store/modules/subtasks'
import { filterSubTasksForRemove } from 'store/sagas/utils'
import { deleteSubTaskSaga } from './index'

describe('run tests with saga', () => {
  it('createTaskSaga test', () => {
    const action = { payload: 12 }
    const deletedElement = {
      id: '60f5c6c9-1746-43b2-a9c5-3ab530655325',
      title: 'test',
      createTime: 14782397423,
      taskId: '60f5c6c9-1746-43b2-a9c5-3ab530655000',
    }

    const elementForSaving = {
      id: '8ede28e3-09fa-4199-bf0e-cb23c5640442',
      title: 'test',
      createTime: 14782397423,
      taskId: '60f5c6c9-1746-43b2-a9c5-3ab530655000',
    }

    testSaga(deleteSubTaskSaga, action)
      .next()
      .put(subTasksActions.deletingStart())
      .next()
      .call(deleteSubtask, action.payload)
      .next()
      .select(getSubTasksData)
      .next([deletedElement])
      .call(filterSubTasksForRemove, [deletedElement], action.payload)
      .next(elementForSaving)
      .put(subTasksActions.setSubtasks(elementForSaving))
      .next()
      .select(getSubTasksData)
  })
})
