import { testSaga } from 'redux-saga-test-plan'
import { createTask, fetchTasks } from 'api/tasks'
import { actions as tasksActions } from 'store/modules/tasks'
import { fetchTasksSaga, createTaskSaga } from './index'

describe('run tests with saga', () => {
  it('fetchTask test', () => {
    testSaga(fetchTasksSaga)
      .next()
      .put(tasksActions.loadingStart())
      .next()
      .call(fetchTasks)
      .next()
      .put(tasksActions.setTasks())
      .next()
      .put(tasksActions.loadingDone())
      .next()
      .isDone()
  })

  it('createTaskSaga test', () => {
    testSaga(createTaskSaga)
      .next()
      .put(tasksActions.loadingStart())
      .next()
      .call(createTask)
      .next()
      .fork(fetchTasksSaga)
      .next()
      .put(tasksActions.loadingDone())
      .next()
      .isDone()
  })
})
