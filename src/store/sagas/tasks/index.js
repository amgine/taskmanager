import { takeLatest, call, put, fork, select } from 'redux-saga/effects'
import { fetchTasks, createTask, deleteTask } from 'api/tasks'
import { dynamicSort } from 'utils/sort'
import { actions as tasksActions, actions as taskActions, getTasksData } from 'store/modules/tasks'
import { getActiveTaskId } from 'store/modules/subtasks'

export function* fetchTasksSaga() {
  try {
    yield put(taskActions.loadingStart())
    const data = yield call(fetchTasks)

    yield put(taskActions.setTasks(data))
  } catch (e) {
    console.log(e)
  } finally {
    yield put(taskActions.loadingDone())
  }
}

export function* createTaskSaga() {
  try {
    yield put(taskActions.loadingStart())
    yield call(createTask)

    yield fork(fetchTasksSaga)
  } catch (e) {
    console.log(e)
  } finally {
    yield put(taskActions.loadingDone())
  }
}

export function* removeTask() {
  try {
    const activeTaskId = yield select(getActiveTaskId)

    yield call(deleteTask, activeTaskId)

    const tasks = yield select(getTasksData)

    const changedTasks = tasks.filter(task => task.id !== activeTaskId)

    yield put(taskActions.setTasks(changedTasks))
  } catch (e) {
    console.log(e)
  }
}

function* sortByCriteriaSaga({ payload }) {
  const tasks = yield select(getTasksData)
  const changedTasks = [...tasks].sort(dynamicSort(payload))

  yield put(taskActions.setTasks(changedTasks))
}

export default function* root() {
  yield takeLatest(taskActions.fetchTasks, fetchTasksSaga)
  yield takeLatest(taskActions.createTask, createTaskSaga)
  yield takeLatest(tasksActions.sortByCriteria, sortByCriteriaSaga)
}
