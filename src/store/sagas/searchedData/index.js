import { all, call, put, debounce } from 'redux-saga/effects'
import { fetchAllSubTasks } from 'api/subTasks'
import { fetchTasks } from 'api/tasks'
import { actions as searchedActions } from '../../modules/searchedData'
import { getDataByCriteria } from '../utils'

export function* searchDataByCriteriaSaga({ payload }) {
  try {
    yield put(searchedActions.loadingStart())
    const [subTasks, tasks] = yield all([call(fetchAllSubTasks), call(fetchTasks)])

    const searchedData = yield call(getDataByCriteria, tasks, subTasks, payload)

    yield put(searchedActions.setSearchedData(searchedData))
  } catch (e) {
    console.log(e)
  } finally {
    yield put(searchedActions.loadingDone())
  }
}

export default function* root() {
  yield debounce(1000, searchedActions.searchByCriteria, searchDataByCriteriaSaga)
}
