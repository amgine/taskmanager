import { call } from 'redux-saga/effects'
import { testSaga } from 'redux-saga-test-plan'
import { actions as searchedActions } from 'store/modules/searchedData'
import { fetchAllSubTasks } from 'api/subTasks'
import { fetchTasks } from 'api/tasks'
import { searchDataByCriteriaSaga } from './index'

describe('run searched data tests with saga', () => {
  it('searchDataByCriteriaSaga test', () => {
    const action = { payload: { title: 'title' } }

    const tasks = [
      {
        id: 'efbe0e00-37fa-4f36-8ff7-a78e4254df4d',
        title: 'test',
        createTime: 14782397423,
      },
    ]

    const subTasks = [
      {
        id: '60f5c6c9-1746-43b2-a9c5-3ab530655325',
        title: 'test',
        createTime: 14782397423,
        taskId: '60f5c6c9-1746-43b2-a9c5-3ab530655000',
      },
    ]

    testSaga(searchDataByCriteriaSaga, action)
      .next()
      .put(searchedActions.loadingStart())
      .next()
      .all([call(fetchAllSubTasks), call(fetchTasks)])
      .next(tasks, subTasks)
  })
})
