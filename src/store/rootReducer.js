import { createBrowserHistory } from 'history'
import { combineReducers } from '@reduxjs/toolkit'
import { connectRouter } from 'connected-react-router'
import { reducer as tasks } from './modules/tasks'
import { reducer as subTasks } from './modules/subtasks'
import { reducer as searchedData } from './modules/searchedData'

const history = createBrowserHistory()

const reducer = combineReducers({
  router: connectRouter(history),
  tasks,
  subTasks,
  searchedData,
})

export { history }

export default reducer
