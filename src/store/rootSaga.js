import { all, fork } from 'redux-saga/effects'
import tasks from './sagas/tasks'
import subTasks from './sagas/subTasks'
import searchedData from './sagas/searchedData'

export default function* () {
  yield all([fork(tasks), fork(subTasks), fork(searchedData)])
}
