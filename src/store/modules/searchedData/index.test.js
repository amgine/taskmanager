import { reducer, actions, DEFAULT_STATE } from './index'

describe('run searchedData reducer tests', () => {
  it('should return default state', () => {
    expect(reducer(undefined, {})).toStrictEqual(DEFAULT_STATE)
  })

  it('should set isLoading true', () => {
    expect(reducer(DEFAULT_STATE, actions.loadingStart())).toStrictEqual({
      ...DEFAULT_STATE,
      isLoading: true,
    })
  })

  it('should set isLoading false', () => {
    expect(reducer(DEFAULT_STATE, actions.loadingDone())).toStrictEqual({
      ...DEFAULT_STATE,
      isLoading: false,
    })
  })
})
