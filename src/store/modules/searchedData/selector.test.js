import { getSearchedData } from './selector'
import { DEFAULT_STATE } from './index'

describe('run subtasks selector tests', () => {
  it('should return state for subtask entity', () => {
    const state = {
      searchedData: {
        ...DEFAULT_STATE,
      },
    }

    expect(getSearchedData(state)).toStrictEqual({
      ...DEFAULT_STATE,
    })
  })
})
