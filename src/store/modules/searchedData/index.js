import { createSlice } from '@reduxjs/toolkit'

export * from './selector'

export const DEFAULT_STATE = {
  data: [],
  isLoading: false,
}

export const { reducer, actions } = createSlice({
  name: 'searchedData',
  initialState: DEFAULT_STATE,
  reducers: {
    loadingStart: state => ({
      ...state,
      isLoading: true,
    }),
    fetchTasks: state => state,
    setSearchedData: (state, { payload: data = [] }) => ({
      ...state,
      data,
    }),
    searchByCriteria: state => state,
    loadingDone: state => ({
      ...state,
      isLoading: false,
    }),
  },
})
