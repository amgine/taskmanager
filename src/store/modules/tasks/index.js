import { createSlice } from '@reduxjs/toolkit'

export * from './selector'

export const DEFAULT_STATE = {
  data: [],
  isLoading: false,
}

export const { reducer, actions } = createSlice({
  name: 'tasks',
  initialState: DEFAULT_STATE,
  reducers: {
    loadingStart: state => ({
      ...state,
      isLoading: true,
    }),
    fetchTasks: state => state,
    setTasks: (state, { payload: data = [] }) => ({
      ...state,
      data,
    }),
    createTask: state => state,
    sortByCriteria: state => state,
    loadingDone: state => ({
      ...state,
      isLoading: false,
    }),
  },
})
