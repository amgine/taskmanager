import { reducer, actions, DEFAULT_STATE } from './index'

describe('run tasks reducer tests', () => {
  it('should return default state', () => {
    expect(reducer(undefined, {})).toStrictEqual(DEFAULT_STATE)
  })

  it('should return default state fetchTasks', () => {
    expect(reducer(undefined, actions.fetchTasks())).toStrictEqual(DEFAULT_STATE)
  })

  it('should set isLoading true', () => {
    expect(reducer(DEFAULT_STATE, actions.loadingStart())).toStrictEqual({
      ...DEFAULT_STATE,
      isLoading: true,
    })
  })

  it('should set isLoading false', () => {
    expect(reducer(DEFAULT_STATE, actions.loadingDone())).toStrictEqual({
      ...DEFAULT_STATE,
      isLoading: false,
    })
  })

  it('should set tasks data', () => {
    const dataForSet = [
      { id: '60f5c6c9-1746-43b2-a9c5-3ab530655325', title: 'test', createTime: 14782397423 },
    ]

    expect(reducer(DEFAULT_STATE, actions.setTasks(dataForSet))).toStrictEqual({
      ...DEFAULT_STATE,
      data: [
        { id: '60f5c6c9-1746-43b2-a9c5-3ab530655325', title: 'test', createTime: 14782397423 },
      ],
    })
  })
})
