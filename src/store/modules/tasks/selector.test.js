import { getTasksData, getTasks } from './selector'

describe('run tasks selector tests', () => {
  it('should return state for task entity', () => {
    const state = {
      tasks: {
        data: [],
        isLoading: false,
      },
    }

    expect(getTasks(state)).toStrictEqual({
      data: [],
      isLoading: false,
    })
  })

  it('should return data', () => {
    const state = {
      tasks: {
        data: [],
        isLoading: false,
      },
    }

    expect(getTasksData(state)).toStrictEqual([])
  })
})
