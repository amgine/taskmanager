export const getTasks = state => state.tasks

export const getTasksData = state => state.tasks?.data || []
