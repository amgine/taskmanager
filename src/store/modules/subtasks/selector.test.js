import { getSubTasksData, getSubTasks, getActiveTaskId } from './selector'
import { DEFAULT_STATE } from './index'

describe('run subtasks selector tests', () => {
  it('should return state for subtask entity', () => {
    const state = {
      subTasks: {
        ...DEFAULT_STATE,
      },
    }

    expect(getSubTasks(state)).toStrictEqual({
      ...DEFAULT_STATE,
    })
  })

  it('should return active taskId', () => {
    const state = {
      subTasks: {
        ...DEFAULT_STATE,
        activeTaskId: '111',
      },
    }

    expect(getActiveTaskId(state)).toBe('111')
  })

  it('should return subTasks data', () => {
    const state = {
      subTasks: {
        ...DEFAULT_STATE,
        data: [{}],
      },
    }

    expect(getSubTasksData(state)).toStrictEqual([{}])
  })
})
