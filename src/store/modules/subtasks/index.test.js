import { reducer, actions, DEFAULT_STATE } from './index'

describe('run subtasks reducer tests', () => {
  it('should return default state', () => {
    expect(reducer(undefined, {})).toStrictEqual(DEFAULT_STATE)
  })

  it('should set isLoading true', () => {
    expect(reducer(DEFAULT_STATE, actions.loadingStart('Run loading'))).toStrictEqual({
      ...DEFAULT_STATE,
      isLoading: true,
    })
  })

  it('should set isDeleting true', () => {
    expect(reducer(DEFAULT_STATE, actions.deletingStart())).toStrictEqual({
      ...DEFAULT_STATE,
      isDeleting: true,
    })
  })

  it('should set isDeleting false', () => {
    expect(reducer(DEFAULT_STATE, actions.deletingDone())).toStrictEqual({
      ...DEFAULT_STATE,
      isDeleting: false,
    })
  })

  it('should set subtasks data', () => {
    const dataForSet = [
      {
        id: '60f5c6c9-1746-43b2-a9c5-3ab530655325',
        title: 'test',
        createTime: 14782397423,
        taskId: '60f5c6c9-1746-43b2-a9c5-3ab530655000',
      },
    ]

    expect(reducer(DEFAULT_STATE, actions.setSubtasks(dataForSet))).toStrictEqual({
      ...DEFAULT_STATE,
      data: [
        {
          id: '60f5c6c9-1746-43b2-a9c5-3ab530655325',
          title: 'test',
          createTime: 14782397423,
          taskId: '60f5c6c9-1746-43b2-a9c5-3ab530655000',
        },
      ],
    })
  })

  it('should set active task', () => {
    expect(
      reducer(DEFAULT_STATE, actions.setActiveTask('60f5c6c9-1746-43b2-a9c5-3ab530655111')),
    ).toStrictEqual({
      ...DEFAULT_STATE,
      activeTaskId: '60f5c6c9-1746-43b2-a9c5-3ab530655111',
    })
  })
})
