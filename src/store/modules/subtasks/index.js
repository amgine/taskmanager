import { createSlice } from '@reduxjs/toolkit'

export * from 'store/modules/subtasks/selector'

export const DEFAULT_STATE = {
  activeTaskId: null,
  data: [],
  isLoading: false,
  isDeleting: false,
}

export const { reducer, actions } = createSlice({
  name: 'subtasks',
  initialState: DEFAULT_STATE,
  reducers: {
    loadingStart: state => ({
      ...state,
      isLoading: true,
    }),
    fetchSubTasks: state => state,
    deleteSubTask: state => state,
    setSubtasks: (state, { payload: data }) => ({
      ...state,
      data,
    }),
    setActiveTask: (state, { payload: activeTaskId }) => ({
      ...state,
      activeTaskId,
    }),
    loadingDone: state => ({
      ...state,
      isLoading: false,
    }),
    deletingStart: state => ({
      ...state,
      isDeleting: true,
    }),
    deletingDone: state => ({
      ...state,
      isDeleting: false,
    }),
  },
})
