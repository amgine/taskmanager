export const getSubTasks = state => state.subTasks

export const getSubTasksData = state => state.subTasks?.data || []

export const getActiveTaskId = state => state.subTasks?.activeTaskId
