import { useEffect, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getTasks, actions as tasksActions } from 'store/modules/tasks'
import { getSubTasks, actions as subTasksActions } from 'store/modules/subtasks'
import { actions as searchedActions, getSearchedData } from 'store/modules/searchedData'
import { FIND_BY_ELEMENTS } from 'constants/index'
import { getSorting } from '../../utils/sort'

export default () => {
  const dispatch = useDispatch()
  const { data: tasks, isLoading } = useSelector(getTasks)
  const {
    data: subTasks,
    isLoading: isSubTasksLoading,
    activeTaskId,
    isDeleting: isSubTaskDeleting,
  } = useSelector(getSubTasks)
  const { data: searchedData, isLoading: isSearchedDataLoading } = useSelector(getSearchedData)

  const [findBy, setFindBy] = useState(FIND_BY_ELEMENTS.TITLE)

  const toggleFindBy = useCallback(
    currentFindBy => {
      if (currentFindBy === findBy) {
        return
      }
      setFindBy(currentFindBy)
    },
    [findBy],
  )

  useEffect(() => {
    dispatch(tasksActions.fetchTasks())
  }, [dispatch])

  const createTask = useCallback(() => {
    dispatch(tasksActions.createTask())
  }, [dispatch])

  const fetchSubtasks = useCallback(
    id => {
      dispatch(subTasksActions.fetchSubTasks(id))
    },
    [dispatch],
  )

  const deleteSubTask = useCallback(
    subTaskId => {
      dispatch(subTasksActions.deleteSubTask(subTaskId))
    },
    [dispatch],
  )

  const [sorting, setSorting] = useState({ field: undefined, isNotReversed: true })

  const sortByCriteria = useCallback(
    field => {
      if (!sorting.field) {
        setSorting({ ...sorting, field })

        return
      }

      if (sorting.field === field) {
        setSorting({ ...sorting, isNotReversed: !sorting.isNotReversed })

        return
      }

      setSorting({ field, isNotReversed: true })
    },
    [sorting.field, sorting.isNotReversed, dispatch],
  )

  useEffect(() => {
    dispatch(tasksActions.sortByCriteria(getSorting(sorting.field, sorting.isNotReversed)))
  }, [sorting.field, sorting.isNotReversed, dispatch])

  const [searchInput, setSearchInput] = useState('')
  const searchByCriteria = useCallback(e => {
    const {
      target: { value: title },
    } = e

    setSearchInput(title)
  }, [])

  useEffect(() => {
    if (!searchInput) {
      return
    }

    dispatch(searchedActions.searchByCriteria({ [findBy]: searchInput }))
  }, [dispatch, searchInput, findBy])

  return {
    tasks,
    createTask,
    isLoading,

    activeTaskId,
    subTasks,
    fetchSubtasks,
    isSubTasksLoading,
    isSubTaskDeleting,

    deleteSubTask,
    sortByCriteria,
    searchByCriteria,
    searchInput,
    searchedData,
    isSearchedDataLoading,

    toggleFindBy,
    findBy,
  }
}
