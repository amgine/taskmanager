import styled from 'styled-components'

export const Wrapper = styled.div`
  font-size: 1rem;
  margin: 3.125rem;
`

export const TitleWrapper = styled.div`
  margin-bottom: 0.625rem;
`

export const Item = styled.li`
  margin: 0 0 1rem 0;
`

export const Title = styled.span`
  font-size: 1.125rem;
  font-weight: 600;
  background-color: #f1e9e9;
  padding: 0.1875rem;
`

export const CreatedTime = styled.p`
  font-size: 0.875rem;
  font-family: 'Consolas, monaco', monospace;
  padding-left: 0.3rem;
  padding-top: 0.3rem;
`
