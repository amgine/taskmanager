import styled from 'styled-components'

export const Wrapper = styled.div`
  padding: 0 0 0 3.125rem;
  font-size: 1rem;
`

export const Item = styled.li`
  margin: 0.9375rem 0;
  color: ${({ color }) => color || 'darkblue'};
  opacity: ${({ opacity }) => opacity || 0.6};
  font-weight: 700;
  font-family: 'Consolas, monaco', monospace;
  font-size: 1.25rem;
`
