import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'
import MarginLeft1 from 'components/MarginLeft1'
import { Wrapper, Item } from './SubTaskList.styles'

const SubTaskList = ({ isSubTasksLoading, isSubTaskDeleting, subTasks, deleteSubTask }) => (
  <Wrapper>
    {isSubTasksLoading && <Item>Loading...</Item>}
    {isSubTaskDeleting && (
      <Item color="deeppink" opacity={0.9}>
        Please wait for deleting...
      </Item>
    )}
    {!isSubTasksLoading &&
      subTasks.map(({ title: subTitle, id: subId, labels }) => (
        <li key={subId}>
          {subTitle}
          <MarginLeft1>
            <Button
              // eslint-disable-next-line react-perf/jsx-no-new-function-as-prop
              onClick={() => deleteSubTask(subId)}
              disabled={isSubTaskDeleting}
              variant="danger"
              size="sm"
            >
              delete subtask
            </Button>
          </MarginLeft1>
          {labels.length && (
            <MarginLeft1 display="block">
              <ul>
                {labels.map(label => (
                  <li key={label}>{label}</li>
                ))}
              </ul>
            </MarginLeft1>
          )}
        </li>
      ))}
  </Wrapper>
)

SubTaskList.defaultProps = {
  isSubTasksLoading: false,
  isSubTaskDeleting: false,
}

SubTaskList.propTypes = {
  subTasks: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  deleteSubTask: PropTypes.func.isRequired,
  isSubTasksLoading: PropTypes.bool,
  isSubTaskDeleting: PropTypes.bool,
}

export default memo(SubTaskList)
