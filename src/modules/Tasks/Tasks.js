import React from 'react'
import useTasks from 'modules/Tasks/useTasks'
import Button from 'components/Button'
import Header from 'components/Header'
import { convertUTtoDate } from 'utils/time'
import MarginLeft1 from 'components/MarginLeft1'
import { Wrapper, Item, Title, CreatedTime, TitleWrapper } from './Tasks.styles'
import SubTaskList from './components/SubTaskList/SubTaskList'

const Tasks = () => {
  const {
    tasks,
    createTask,
    fetchSubtasks,
    isLoading,
    activeTaskId,
    subTasks,
    isSubTasksLoading,
    isSubTaskDeleting,
    deleteSubTask,
    sortByCriteria,
    searchByCriteria,
    searchInput,
    searchedData,
    isSearchedDataLoading,
    toggleFindBy,
    findBy,
  } = useTasks()

  return (
    <Wrapper>
      <Header
        findBy={findBy}
        toggleFindBy={toggleFindBy}
        searchedData={searchedData}
        isSearchedDataLoading={isSearchedDataLoading}
        searchInput={searchInput}
        sortByCriteria={sortByCriteria}
        searchByCriteria={searchByCriteria}
      />
      <ul>
        {tasks.map(({ title, id, createTime }) => (
          <Item key={id}>
            <TitleWrapper>
              <Title>{title}</Title>
              <MarginLeft1>
                {' '}
                <Button
                  // eslint-disable-next-line react-perf/jsx-no-new-function-as-prop
                  onClick={() => fetchSubtasks(id)}
                  size="sm"
                >
                  {activeTaskId === id ? 'close task' : 'open task'}
                </Button>
              </MarginLeft1>
              <CreatedTime>Created at {convertUTtoDate(createTime)}</CreatedTime>
            </TitleWrapper>
            {activeTaskId === id && (
              <SubTaskList
                isSubTaskDeleting={isSubTaskDeleting}
                isSubTasksLoading={isSubTasksLoading}
                subTasks={subTasks}
                deleteSubTask={deleteSubTask}
              />
            )}
          </Item>
        ))}
      </ul>
      <Button disabled={isLoading} onClick={createTask} variant="success" size="lg">
        create task
      </Button>
    </Wrapper>
  )
}

export default Tasks
