import Storage from 'utils/storage'
import delay from 'utils/delay'

export default delay(taskId => {
  const tasks = Storage.tasks.get()

  Storage.tasks.set(tasks.filter(st => st.id !== taskId))
})
