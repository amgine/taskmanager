export function getSorting(f, fb) {
  let flag = ''

  if (!fb) {
    flag = '-'
  }

  return `${flag}${f}`
}

export function dynamicSort(property) {
  let sortOrder = 1

  if (property[0] === '-') {
    sortOrder = -1
    // eslint-disable-next-line no-param-reassign
    property = property.substr(1)
  }

  return function (a, b) {
    // eslint-disable-next-line no-nested-ternary
    const result = a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0

    return result * sortOrder
  }
}
