import styled from 'styled-components'

export const Wrapper = styled.h1`
  color: ${({ color }) => color || '#5151e7'};
  padding: ${({ padding }) => padding || '0 0 0.9375rem 0'};
  font-weight: 700;
`
