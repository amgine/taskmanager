import styled from 'styled-components'

export const StyledMarginLeft1 = styled.div`
  margin: 0 0 0 1rem;
  display: ${({ display }) => display || 'inline-block'};
`
