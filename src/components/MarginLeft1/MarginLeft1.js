import React from 'react'
import { StyledMarginLeft1 } from './MarginLeft1.styles'

const MarginLeft1 = props => <StyledMarginLeft1 {...props} />

export default MarginLeft1
