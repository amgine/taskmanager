import styled from 'styled-components'

export const StyledInput = styled.input`
  width: ${({ width }) => width || '18.75rem'};
  padding: ${({ padding }) => padding || '0.75rem 1.25rem'};
  margin: ${({ margin }) => margin || '0.5rem 0 0.9375rem'};
  display: ${({ display }) => display || 'inline-block'};
  border: ${({ border }) => border || '0.0625rem solid #ccc'};
  border-radius: ${props => props.borderRadius || '0.25rem'};
  box-sizing: ${props => props.boxSizing || 'border-box'};
`
