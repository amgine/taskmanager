/* eslint-disable react-perf/jsx-no-new-function-as-prop,jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions,react-perf/jsx-no-new-object-as-prop */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FIND_BY_ELEMENTS, SORTED_ELEMENTS } from 'constants/index'
import Input from 'components/Input'
import Button from 'components/Button'
import Title from 'components/Title'
import {
  Wrapper,
  SearchedDataWrapper,
  SearchedDataElement,
  SearchedDataButtons,
  SearchMessage,
  SortWrapper,
} from './Header.styles'

const Header = ({
  sortByCriteria,
  findBy,
  toggleFindBy,
  searchByCriteria,
  searchInput,
  searchedData,
  isSearchedDataLoading,
}) => {
  const [focus, setFocus] = useState(false)

  return (
    <Wrapper>
      <Title color="#0000ff">What do you want to do</Title>
      <Input
        type="text"
        onChange={searchByCriteria}
        value={searchInput}
        placeholder="Search..."
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
      />
      <SearchedDataButtons>
        <Button
          variant={FIND_BY_ELEMENTS.TITLE === findBy ? 'info' : 'light'}
          border={FIND_BY_ELEMENTS.TITLE !== findBy && '0.0625rem solid'}
          onClick={() => toggleFindBy(FIND_BY_ELEMENTS.TITLE)}
        >
          {' '}
          search by title
        </Button>
      </SearchedDataButtons>
      <SearchedDataButtons>
        <Button
          variant={FIND_BY_ELEMENTS.LABEL === findBy ? 'info' : 'light'}
          border={FIND_BY_ELEMENTS.LABEL !== findBy && '0.0625rem solid'}
          onClick={() => toggleFindBy(FIND_BY_ELEMENTS.LABEL)}
        >
          {' '}
          search by subtask label
        </Button>
      </SearchedDataButtons>
      {isSearchedDataLoading && <SearchMessage>Search is in progress...</SearchMessage>}
      {(focus || searchInput) && !!searchedData.length && (
        <SearchedDataWrapper>
          {searchedData.map(el => (
            <SearchedDataElement key={el.id}>
              {el.title} {el.taskId ? '-isSubtask' : '-task'}
            </SearchedDataElement>
          ))}
        </SearchedDataWrapper>
      )}
      <SortWrapper>
        Sort by
        <SearchedDataButtons>
          <Button onClick={() => sortByCriteria(SORTED_ELEMENTS.TITLE)} variant="light">
            Title
          </Button>
        </SearchedDataButtons>
        <SearchedDataButtons>
          <Button onClick={() => sortByCriteria(SORTED_ELEMENTS.CREATE_TIME)} variant="light">
            Time
          </Button>
        </SearchedDataButtons>
      </SortWrapper>
    </Wrapper>
  )
}

Header.propTypes = {
  searchedData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isSearchedDataLoading: PropTypes.bool.isRequired,
  searchInput: PropTypes.string.isRequired,
  sortByCriteria: PropTypes.func.isRequired,
  searchByCriteria: PropTypes.func.isRequired,
  findBy: PropTypes.oneOf(Object.values(FIND_BY_ELEMENTS)).isRequired,
  toggleFindBy: PropTypes.func.isRequired,
}

export default Header
