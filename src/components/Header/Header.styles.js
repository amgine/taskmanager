import styled from 'styled-components'

export const Wrapper = styled.div`
  margin: 0.625rem 0;
`

export const SearchedDataWrapper = styled.ul`
  background: #ffff;
  padding: 0.75rem;
  border: 0.0625rem solid #000;
  border-radius: 0.3125rem;
  margin-bottom: 0.625rem;
  max-width: 62.5rem;
`

export const SortWrapper = styled.div`
  font-weight: 700;
`

export const SearchedDataButtons = styled.p`
  display: inline-block;
  margin-left: 0.625rem;
`

export const SearchMessage = styled.p`
  font-family: 'Consolas, monaco', monospace;
  padding: 0.5rem 1rem 1rem;
`

export const SearchedDataElement = styled.li`
  padding-bottom: 0.3125rem;

  &:hover {
    background: #ecec64;
  }
`
