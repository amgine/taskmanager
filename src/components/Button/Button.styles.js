/* eslint-disable no-shadow */
import styled, { css } from 'styled-components'

export const Wrapper = styled.button.attrs(() => ({
  outlined: true,
}))`
  background-color: ${props => props.backColor || '#007bff'};
  color: ${({ color }) => color || '#fff'};
  border: ${({ border }) => border || 'none'};
  border-radius: ${props => props.borderRadius || '0.4rem'};
  padding: ${({ padding }) => padding || '0.375rem 0.75rem'};
  font-size: ${props => props.fontSize || '1rem'};
  cursor: pointer;
  margin: ${({ margin }) => margin || 0};
  align-self: ${props => props.align || 'stretch'};

  &:hover {
    background-color: ${props => props.backColor || '#0069d9'};
  }

  ${props =>
    props.variant === 'light' &&
    css`
      color: ${({ color }) => color || '#212529'};
      background-color: ${props => props.backColor || '#f8f9fa'};

      &:hover {
        background: ${props => props.backgroundHover || '#e2e6ea'};
      }
    `}

  ${props =>
    props.variant === 'danger' &&
    css`
      color: ${({ color }) => color || '#fff'};
      background-color: ${props => props.backColor || '#dc3545'};

      &:hover {
        background: ${props => props.backgroundHover || '#c82333'};
      }
    `}

  ${props =>
    props.variant === 'success' &&
    css`
      color: ${({ color }) => color || '#fff'};
      background-color: ${props => props.backColor || '#28a745'};

      &:hover {
        background: ${props => props.backgroundHover || '#218838'};
      }
    `}

  ${props =>
    props.variant === 'info' &&
    css`
      color: ${({ color }) => color || '#fff'};
      background-color: ${props => props.backColor || '#17a2b8'};

      &:hover {
        background: ${props => props.backgroundHover || '#138496'};
      }
    `}

  ${props =>
    props.size === 'sm' &&
    css`
      font-size: 0.875rem;
      padding: 0.25rem 0.5rem;
    `}

  ${props =>
    props.size === 'lg' &&
    css`
      font-size: 1.25rem;
      padding: 0.5rem 1rem;
    `}

  &:disabled {
    opacity: 0.4;
    cursor: none;
  }
`
